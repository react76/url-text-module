#!/bin/bash
# Update requirements.txt with newly downloaded packages
while getopts ":m:t:" option;
do
    case ${option} in
        m) MESSAGE=${OPTARG};;
        t) TAG=${OPTARG};;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            exit 1
            ;;
        :)
            echo "Option -$OPTARG requires an argument." >&2
            exit 1
            ;;
    esac
done
git pull
git add -A
git commit -m "$MESSAGE"
git tag $TAG
git push
git push --tags