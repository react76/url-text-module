rm -r build
rm -r dist
pip uninstall url_text_module
pipenv run pip freeze > requirements.txt
python setup.py bdist_wheel sdist
pip install -e .
python